#ifndef CHECKERSENSOR_H
#define CHECKERSENSOR_H

struct coords{                                      //Cell location of a sensor
                                                    // x = row, y = column
    int x, y;
};

struct checkerSensor{                                      //Sensor information

    QString key;
    QVector<coords> coordinates;                    //Compilation of one sensor's different locations
    int wordint;
    int frameint;
};

#endif // CHECKERSENSOR_H
